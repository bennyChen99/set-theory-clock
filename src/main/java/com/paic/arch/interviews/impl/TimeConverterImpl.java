package com.paic.arch.interviews.impl;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.interviews.time.bean.CustomTime;
import com.paic.arch.interviews.time.exception.CustomTimeException;

/**
 * @author Benny
 * @version V1.0
 * @Description: 时间转换实现类  *
 * 1. 由于 小时 和 分钟 的计算方式雷同，故计算结果 共用 wrapResultString 方法
 * 2. 分钟 的计算结果，需要根据 特殊要求， 进行变色处理，使用 convertResultsColor 方法 进行处理
 * @Date 2018/4/10
 */

public class TimeConverterImpl implements TimeConverter {
    public static final String YELLOE = "Y";
    public static final String RED = "R";

    public static final String ORANGE = "O";//表示空白

    //根据系统平台得到换行符
    //参阅：
    //windows下的文本文件换行符:\r\n linux/unix下的文本文件换行符:\r Mac下的文本文件换行符:\n
    public static final String BREAK_LINE = System.getProperty("line.separator", "/n");;

    @Override
    public String convertTime(String aTime) {

        //先将字符串转换为对应的时分秒
        //提前在所有异常情况在转换为 自定义Time类的过程中 提前检测
        CustomTime time = null;
        try {
            time = new CustomTime(aTime);
        } catch (CustomTimeException e) {
            e.printStackTrace();
        }

        //分别获取 秒、时、分 的计算结果（以及颜色）
        String ret = getSecondsClock(time.getSeconds())
                + getHourClock(time.getHour())
                + getMinuteClock(time.getMinute());

        return ret.trim();
    }


    private String getHourClock(int hour) {
        String ret = "";//返回结果

        int placeHolderRow01 = hour / 5;//第一批，一个R为5小时
        int placeHolderRow02 = hour % 5;//第二排，一个R为1小时

        ret += wrapResultString(placeHolderRow01, 4, RED);
        ret += wrapResultString(placeHolderRow02, 4, RED);
        return ret;
    }

    private String getMinuteClock(int minute) {
        String ret = "";

        int placeHolderRow01 = minute / 5;
        int placeHolderRow02 = minute % 5;

        ret += wrapResultString(placeHolderRow01, 11, YELLOE);
        ret = convertResultsColor(ret, 3, YELLOE.charAt(0), RED.charAt(0));// 逢3，则为 需变色的位置

        ret += wrapResultString(placeHolderRow02, 4, YELLOE);
        return ret;
    }

    private String getSecondsClock(int seconds) {
        if (0 == seconds % 2) {
            return YELLOE + BREAK_LINE;
        }
        return "O" + BREAK_LINE;
    }

    /**
     * 将计算结果转换为字符串
     *
     * @param size
     * @param length
     * @param color
     * @return
     */
    private static String wrapResultString(int size, int length, String color) {
        int tmp = size;//中间变量
        String ret = "";
        while (tmp >= 1) {
            ret += color;
            tmp--;
        }
        tmp = length - size;
        while (tmp >= 1) {
            ret += ORANGE;
            tmp--;
        }
        ret = ret + BREAK_LINE;
        return ret;
    }


    /**
     * 将计算结果转换为颜色
     *
     * @param orgResultStr
     * @param changeModNum
     * @param orgColor
     * @param change2Color
     * @return
     */
    private static String convertResultsColor(String orgResultStr, int changeModNum, char orgColor, char change2Color) {

        char[] cs = orgResultStr.toCharArray();
        int size = cs.length;

        for (int i = 0; i < size; i++) {
            //此位置需要变色
            if ((i + 1) % changeModNum == 0) {
                //如果该位置颜色符合变色条件，则进行 换色处理
                if (cs[i] == orgColor) {
                    cs[i] = change2Color;
                }
            }
        }
        return new String(cs);

    }
}