package com.paic.arch.interviews.time.config;

/**
 * @author Benny
 * @version V1.0
 * @Description: 自定义Time常量类   *
 * @Date 2018/4/10
 */
public class TimeConstants {
    public static final String HOUR = "HH";
    public static final String MINUTE = "mm";
    public static final String SECONDS = "ss";
}
