package com.paic.arch.interviews.time.util;

import com.paic.arch.interviews.time.config.TimeConstants;
import com.paic.arch.interviews.time.exception.CustomTimeException;
import com.paic.arch.interviews.time.exception.DaoExceptionCodes;
import org.apache.commons.lang.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Benny
 * @version V1.0
 * @Description: 时间转换 工具类  *
 * @Date 2018/4/10
 */


public class TimeUtil {

    //24小时制
    private final static String REG_TIME_FORMAT = "HH:mm:ss";

    private final static String REG_TIME_FORMAT_00 = "00:00:00";
    private final static String REG_TIME_FORMAT_24 = "24:00:00";

    /**
     * 时间格式化函数
     * 注意:由于使用了SimpleDateFormat，为保障线程安全，故上锁处理
     * Quote:  Date formats are not synchronized.
     *   If multiple threads access a format concurrently, it must be synchronized
     * @param time
     *
     * @return String(time format, e.g. HH:mm:ss)
     */
    public static synchronized String regDate(String time) throws CustomTimeException {
        if (StringUtils.isBlank(time)) {
            throw new CustomTimeException(DaoExceptionCodes.ZA020003);
        }

        SimpleDateFormat formatter = new SimpleDateFormat(REG_TIME_FORMAT);

        try {
            // 使用通用 模式 HH:mm:ss
            // 每天的边界点 Midnight 24:00:00
            // 在这个模式下会转换成 Midnight 00:00:00
            if (formatter.format(formatter.parse(time)).equals(time)) {
                return formatter.format(formatter.parse(time));
            } else {
                //处理边界情况
                formatter.applyPattern(REG_TIME_FORMAT_24);
                if (formatter.format(formatter.parse(time)).equals(time)) {
                    return formatter.format(formatter.parse(time));
                }

                //
                throw new CustomTimeException(DaoExceptionCodes.ZA020001);
            }
        } catch (ParseException exp) {
            throw new CustomTimeException(DaoExceptionCodes.ZA020001);
        } catch (Exception exp) {
            throw new CustomTimeException(DaoExceptionCodes.ZA020000);
        }

    }

    /**
     * 时间 映射 处理函数
     *
     * @param time
     *
     * @return MapObject
     */
    public static Map<String, Integer> regDateMap(String time) throws CustomTimeException {
        //使用正则表达式处理输入时间
        //统一样式（时、分、秒 都是为两位）
        // 样例1：23:1:9
        // 输出： 23:01:09
        time = regDate(time);

        Map<String, Integer> map = new HashMap();
        String[] split = time.split(":");
        int li_hour = Integer.parseInt(split[0].trim());
        int li_minute = Integer.parseInt(split[1].trim());
        int li_seconds = Integer.parseInt(split[2].trim());


        map.put(TimeConstants.HOUR, li_hour);
        map.put(TimeConstants.MINUTE, li_minute);
        map.put(TimeConstants.SECONDS, li_seconds);

        return map;

    }
}
