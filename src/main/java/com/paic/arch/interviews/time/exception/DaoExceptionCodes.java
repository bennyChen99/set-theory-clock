package com.paic.arch.interviews.time.exception;

/**
 * @author Benny
 * @version V1.0
 * @Description: 自定义Time异常类 异常代码列表  *
 * @Date 2018/4/10
 */

public class DaoExceptionCodes {
    public static final String ZA020000 = "时间工具类异常! 参阅格式 [HH:mm:ss]";
    public static final String ZA020001 = "格式转换异常! 参阅格式 [HH:mm:ss]";
    public static final String ZA020002 = "输入时间格式异常! 参阅格式 [HH:mm:ss]";
    public static final String ZA020003 = "输入时间为空! 参阅格式 [HH:mm:ss]";
}
