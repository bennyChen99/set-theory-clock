package com.paic.arch.interviews.time.exception;

/**
 * @author Benny
 * @version V1.0
 * @Description: 自定义Time异常类  *
 * @Date 2018/4/10
 */

public class CustomTimeException extends Exception {

    public CustomTimeException(String msg) {
        super(msg);
    }
}