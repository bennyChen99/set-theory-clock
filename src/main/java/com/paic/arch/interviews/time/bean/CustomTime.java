package com.paic.arch.interviews.time.bean;

import com.paic.arch.interviews.time.config.TimeConstants;
import com.paic.arch.interviews.time.exception.CustomTimeException;
import com.paic.arch.interviews.time.util.TimeUtil;

import java.util.Map;

/**
 * @author Benny
 * @version V1.0
 * @Description: 自定义Time实体，采用TimeUtil将输入的时间参数进行格式化，并将 时、分、秒 初始化给 CustomTime构造函数   *
 * @Date 2018/4/10
 */

public class CustomTime {

    public CustomTime(){}

    /**
     * pattern HH:mm:ss
     * @param time
     */
    public CustomTime(String time) throws CustomTimeException {


        Map<String,Integer> map = TimeUtil.regDateMap(time);

        hour = map.get(TimeConstants.HOUR);
        minute = map.get(TimeConstants.MINUTE);
        seconds = map.get(TimeConstants.SECONDS);

    }

    private int hour;
    private int minute;
    private int seconds;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}

